/* Copyright Stanislav Simko, stanislav.simko AT gmail, 2018

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License (LGPL)
 as published by the Free Software Foundation, either version 2.1 
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the Lesser GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.

--------------------------------------------------------------------------
 3rd party libraries (may) use different license
*/
 

////////////////////////////////////////////////////////////////////////////
/*
 * OpenBabel program to convert xyz structure to 
 * fractional coordinates and writing .cif file.
 * Program reads input file (-i), overwrites output 
 * file (-o) and makes use of provided box size (-b).
 * Box dimensions are comma separated 
 * as: x,y,z,alpha,beta,gamma
 */ 
#include <unistd.h>
#include <string.h>
#include <iostream>
#include <openbabel/generic.h>
#include <openbabel/obconversion.h>
#include <openbabel/mol.h>

int main(int argc, char *argv[]){
    char *ifile = NULL;
    char *ofile = NULL;
    char *box = NULL;
    int c;
    int index;
    double x,y,z,alpha,beta,gamma;
    x = y = z = alpha = beta = gamma = NAN;
    while ( (c = getopt(argc,argv,"i:o:b:")) != -1 )
      switch(c){
        case 'i':
          ifile = optarg;
          break;
        case 'o':
          ofile = optarg;
          break;
        case 'b':
          box = optarg;
          break;
    }
    std::ifstream ifs(ifile);
    if (!ifs){
      std::cout << "Could not open input file\n";
      return 1;
    }
    std::ofstream ofs(ofile);
    if (!ofs){
      std::cout << "Could not open output file\n";
      return 1;
    }
    if (!sscanf(box,"%lf,%lf,%lf,%lf,%lf,%lf",
                &x,&y,&z,&alpha,&beta,&gamma)){
        std::cout << "Error while parsing box information\n";
        return 1;
    }
    if (isnan(alpha) or isnan(beta) or isnan(gamma)){
      if (isnan(alpha) && isnan(beta) && isnan(gamma)){
        std::cout << "angles not specified, using 90°\n";
        alpha = beta = gamma = 90.0;
      } if (isnan(alpha) xor isnan(beta) xor isnan(gamma)){
        std::cout << "Error: specified only 2 of three box angles.\n";
        return 1;
      } else if (!isnan(alpha) xor !isnan(beta) xor !isnan(gamma)){
        beta = gamma = alpha;
      }
    }
    OpenBabel::OBConversion conv(&ifs,&ofs);
    OpenBabel::OBMol mol;
    OpenBabel::OBUnitCell *cel = new OpenBabel::OBUnitCell;
    cel->SetData(x,y,z,alpha,beta,gamma);
    cel->SetLatticeType(OpenBabel::OBUnitCell::LatticeType::Rhombohedral);
//    cel.SetData(17.6891,17.6891,17.6891,60.0000,60.0000,60.0000);
    if (conv.SetInAndOutFormats("xyz","cif")){
        conv.Read(&mol);
        // assigning cell to the molecule must happen after
        // reading the molecule, otherwise it will not convert
        // xyz to fractional coordinates
        mol.SetData(cel);
        //conv.Convert();
        conv.Write(&mol);
    }
    ifs.close();
    ofs.close();
    return 0;
}
