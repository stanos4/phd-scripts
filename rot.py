# Copyright Stanislav Simko, stanislav.simko AT gmail, 2018

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License (LGPL)
# as published by the Free Software Foundation, either version 2.1 
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the Lesser GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

#--------------------------------------------------------------------------
# 3rd party libraries (may) use different license
# 

############################################################################
# This program is used to generate random orientations of an input 
# molecule (ligand). It is possible to reject structures where the 
# ligand is closer than a trashold to the optional target molecule.
# Ligand may also be translated by random displacement smaller than
# a treshold. Program tries to generate n rotations/translations
# but it does not try more than m>=n times.


import sys, getopt, os, glob
import openbabel as ob
import numpy as np
try:
    from builtins import input
    bti = True
except:
    bti = False

def usage(pname):
    print(pname,"\n")
    print("-h, --help     : Print this help message")
    print("-i, --input    : Name (path) of the geometry file (file types supported by OpenBabel OBConversion class, e.g. pdb, xyz)")
    print("-o, --output   : [optional] Name (path) of the output file. Without suffix. xyz is used for output.\n"
          "                 Default: 'rotated_[i].pdb'")
    print("-n             : [optional] Number of rotations.\n"
          "                 Default: n=1")
    print("-m             : Maximal number of trials.\n"
          "                 Default: 1000")
    print("-t             : [optional] Name (path) of the target molecule (it must be the same file type as molecule to be rotated")
    print("-d             : Minimal distance between rotated and target molecule")
    print("-r             : [optional] Maximal random displacement of the rotated molecule. Either single value or comma separated\n"
          "                 x,y,z values. No displacements are applied if this value is not provided.")
    print("-f             : Overwrite output files without asking")

def f_main(argv=sys.argv):
    try:
        opts, args = getopt.getopt(argv[1:],"hi:o:n:t:d:r:m:f",["input=","output=","target=","help"])
    except:
        usage(argv[0])
        sys.exit(2)

    infile = None
    outfile = 'rotated_'
    conflict = False
    overwrite = False
    target_inpName = None
    Nums = 1
    treshold = False
    displacement = False
    maxNums = 1000
    for opt,arg in opts:
        if opt in ("-h","--help"):
            usage(sys.argv[0])
            sys.exit(1)
        elif opt in ("-i","--input"):
            if not os.path.isfile(arg):
                sys.exit("File does not exits: {}".format(arg))
            infile = arg
            infile_suffix = os.path.splitext(arg)[1]
        elif opt in ("-o","--output"):
            if glob.glob(arg+"[0-9]*.xyz"):
                conflict = True
            outfile = arg
        elif opt == "-f":
            overwrite = True
        elif opt == "-n":
            Nums = int(arg)
        elif opt in ("-t", "--target"):
            target_inpName = arg
        elif opt == "-d":
            treshold = float(arg)
        elif opt == "-r":
            displacement = list(map(float,arg.split(',')))
            print(displacement)
        elif opt == "-m":
            maxNums = int(arg)

    if conflict:
        if not overwrite:
            if bti:
                answ = input("File(s) '{}' already exists. Overwrite? y/n: ".format(glob.glob(outfile+"[0-9]*.xyz")))
                assert isinstance(answ,str)
            else:
                answ = raw_input("File(s) '{}' already exists. Overwrite? y/n: ".format(glob.glob(outfile+"[0-9]*.xyz")))
                assert isinstance(answ,str)
            if not answ.lower() in ("y","yes"):
                sys.exit("Try again with different output file. Abort.")
    if infile is None:
        usage(sys.argv[0])
        sys.exit(1)

    mol = ob.OBMol()
    obconversion = ob.OBConversion()
    obconversion.SetInAndOutFormats(infile_suffix,"xyz")
    obconversion.ReadFile(mol, infile)

    if target_inpName:
        target_mol = ob.OBMol()
        obconversion.ReadFile(target_mol, target_inpName)
        target_Nats = target_mol.NumAtoms()
        target_coords_ptr = target_mol.GetCoordinates()
        target_coords_tmp = ob.doubleArray_frompointer(target_coords_ptr)
        target_coords = np.array([target_coords_tmp[i] for i in range(3*target_Nats)])
        target_coords = target_coords.reshape((target_Nats,3))

    Nats = mol.NumAtoms()
    orig_coords_ptr = mol.GetCoordinates()
    orig_coords_tmp = ob.doubleArray_frompointer(orig_coords_ptr)
    orig_coords = np.array([orig_coords_tmp[i] for i in range(3*Nats)])
    orig_coords = orig_coords.reshape((Nats,3))
    # calling OBMol::Center(nconf) must actually happen after storing coordinates
    # because it actually moves molecule
    translation = mol.Center(0)

    rnd = ob.OBRandom()
    rnd.TimeSeed()
    vx = ob.vector3(1,0,0)
    vy = ob.vector3(0,1,0)
    vz = ob.vector3(0,0,1)
    tmp_rot = ob.matrix3x3(vx,vy,vz)
    test_coords_ptr = mol.GetCoordinates()
    test_coords_arr = ob.doubleArray_frompointer(test_coords_ptr)
    switch = False
    N_saved = 1
    for i in range(maxNums):
        safe = True
        new_mol = ob.OBMol(mol)
        tmp_rot.randomRotation(rnd)
        a = ob.doubleArray(9)
        tmp_rot.GetArray(a)
        new_mol.Rotate(a)
        new_mol.Translate(translation)
        tmp_transl = np.random.uniform(size=3)
        tmp_transl = tmp_transl * np.array(displacement)
        rand_transl = ob.vector3(tmp_transl[0],tmp_transl[1],tmp_transl[2])
        new_mol.Translate(rand_transl)
        new_coords_ptr = new_mol.GetCoordinates()
        new_coords_tmp = ob.doubleArray_frompointer(new_coords_ptr)
        new_coords = np.array([new_coords_tmp[k] for k in range(3*Nats)])
        new_coords = new_coords.reshape((Nats,3))

        if target_inpName is not None:
            if not treshold:
                sys.exit("Specify the minimal distance between rotated molecule and target")
            for j in range(Nats):
                dist_vecs = target_coords - new_coords[j]
                distances = np.linalg.norm(dist_vecs,axis=1)
                if distances.min() < treshold:
                    safe = False
                    break
        if safe:
            obconversion.WriteFile(new_mol,"{0}{1}.xyz".format(outfile,N_saved))
            N_saved += 1
            if N_saved >= Nums: break
    print("number of structures generated: {}".format(N_saved))

if __name__ == "__main__":
    f_main(sys.argv)

